import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Icon from "./index";

export default {
  title: "yt-ui/Icon",
  component: Icon,
} as ComponentMeta<typeof Icon>;

const Template: ComponentStory<typeof Icon> = (args) => <Icon {...args} />;

export const Icons = () => {
  return (
    <>
      <Icon icon="angle-up" fonttheme="danger"></Icon>
      <Icon icon="angle-down" fonttheme="primary" size="5x"></Icon>
      <Icon icon="angle-left" fonttheme="success" size="lg"></Icon>
      <Icon icon="angle-right" fonttheme="warning" size="10x"></Icon>
    </>
  );
};

export const ThemeIcon = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ThemeIcon.args = {
  fonttheme: "success",
  icon: "angle-right",
};

export const SizeIcon = Template.bind({});
SizeIcon.args = {
  size: "lg",
  icon: "angle-up",
};
