import React from "react";
import { CSSTransition } from "react-transition-group";
import { CSSTransitionProps } from "react-transition-group/CSSTransition";

export type AnimationName =
  | "zoom-in-top"
  | "zoom-in-bottom"
  | "zoom-in-right"
  | "zoom-in-left";

// TODO:這邊有個 props 的 bug 待修復 "in" & "duration" 應為必填項目
type TTransitionProps = {
  /** 這邊可至 global style 自行添加動畫 */
  animation?: AnimationName;
  /** 當 children 有自己的 css transition or 沒有最外層容器時建議設置為 true 以添加確保動畫完整 */
  wrapper?: boolean;
  children: React.ReactNode;
};
type ITransitionProps = TTransitionProps & CSSTransitionProps;

const Transition: React.FC<ITransitionProps> = (props) => {
  const { children, animation, classNames, wrapper, ...restProps } = props;
  return (
    <CSSTransition
      classNames={classNames ? classNames : animation}
      {...restProps}
    >
      {wrapper ? <div>{children}</div> : children}
    </CSSTransition>
  );
};

Transition.defaultProps = {
  appear: true,
  unmountOnExit: true,
  wrapper: false,
};

export default Transition;
