const commonStyle = {
  $commonColors: {
    $black: "#000000",
    $white: "#ffffff",
    $gray: "#d6d6d6",
  },
  $button: {
    $buttonBorder: "1px",
    $buttonLargeFontSize: "1.25rem",
    $buttonDefaultFontSize: "1rem",
    $buttonSmallFontSize: "0.75rem",
    $buttonLargePadding: "0.75rem",
    $buttonDefaultPadding: "0.75rem",
    $buttonSmallPadding: "0.75rem",
    $buttonDisabledOpacity: 0.5,
  },
  $menu: {
    $menuBoxShadow:
      "inset 0 1px 0 rgba(255, 255, 255, 0.15), 0 1px 1px rgba(255, 255, 255, 0.15);",
    $menuBorderBottom: "1px solid #dee2e6;",
    $menuItemPadding: "12px 16px",
    $menuItemActiveBorderBottomWidth: "3px",
  },
};
export const lightTheme = {
  $colors: {
    $primary: "#0d6efd",
    $secondary: "#adb5bd",
    $success: "#198754",
    $info: "#0dcaf0",
    $warning: "#ffc107",
    $danger: "#dc3545",
    $dark: "#000000",
    $light: "#ffffff",
  },
  ...commonStyle,
};

export const darkTheme: Theme = {
  $colors: {
    $primary: "#000000",
    $secondary: "#adb5bd",
    $success: "#198754",
    $info: "#0dcaf0",
    $warning: "#ffc107",
    $danger: "#dc3545",
    $dark: "#ffffff",
    $light: "#000000",
  },
  ...commonStyle,
};

type Theme = typeof lightTheme;
declare module "styled-components" {
  interface DefaultTheme extends Theme {}
}
