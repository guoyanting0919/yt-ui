import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Button from "./index";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "yt-ui/Button",
  component: Button,
  argTypes: { onClick: { action: "clicked" } },
  // parameters: { actions: { argTypesRegex: "^on.*" } },
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
} as ComponentMeta<typeof Button>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Buttons = () => {
  return (
    <>
      <Button btnType="success">ThemeButton</Button>
      <Button size="lg">SizeButton</Button>
      <Button outline>OutlineButton</Button>
      <Button disabled>DisabledButton</Button>
      <Button rounded>RoundedButton</Button>
      <Button icon={"angle-down"} iconPosition={"start"}>
        IconButton
      </Button>
      <Button btnType="link" href="123">
        LinkButton
      </Button>
    </>
  );
};

export const ThemeButton = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
ThemeButton.args = {
  btnType: "success",
  children: "Button",
};

export const SizeButton = Template.bind({});
SizeButton.args = {
  size: "lg",
  children: "Button",
};

export const OutlineButton = Template.bind({});
OutlineButton.args = {
  outline: true,
  children: "Button",
};

export const DisabledButton = Template.bind({});
DisabledButton.args = {
  disabled: true,
  children: "Button",
};

export const RoundedButton = Template.bind({});
RoundedButton.args = {
  rounded: true,
  children: "Button",
};

export const IconButton = Template.bind({});
IconButton.args = {
  icon: "angle-down",
  children: "Button",
};

export const LinkButton = Template.bind({});
LinkButton.args = {
  btnType: "link",
  href: "123",
  children: "Button",
};
