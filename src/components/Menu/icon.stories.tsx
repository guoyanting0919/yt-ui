import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Menu from "./index";
import MenuItem from "./MenuItem";
import SubMenu from "./SubMenu";

export default {
  title: "yt-ui/Menu",
  component: Menu,
  subcomponents: { MenuItem, SubMenu },
  decorators: [
    (Story) => (
      <div style={{ height: "100px" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof Menu>;

export const EmptyMenu: ComponentStory<typeof Menu> = (args) => (
  <Menu {...args}>
    <MenuItem>1.test</MenuItem>
    <MenuItem disabled>2.test2(disabled)</MenuItem>
    <MenuItem>3.test3</MenuItem>
    <MenuItem>4.test4</MenuItem>
  </Menu>
);

export const HorizontalMenu: ComponentStory<typeof Menu> = (args) => {
  return (
    <Menu {...args} defaultIndex="1">
      <MenuItem>1.test</MenuItem>
      <MenuItem disabled>2.test2(disabled)</MenuItem>
      <MenuItem>3.test3</MenuItem>
      <MenuItem>4.test4</MenuItem>
    </Menu>
  );
};

export const VerticalMenu: ComponentStory<typeof Menu> = (args) => {
  return (
    <Menu {...args} mode="vertical">
      <MenuItem>1.test</MenuItem>
      <MenuItem disabled>2.test2(disabled)</MenuItem>
      <MenuItem>3.test3</MenuItem>
      <MenuItem>4.test4</MenuItem>
    </Menu>
  );
};

export const HorizontalMenuWithSubMenu: ComponentStory<typeof Menu> = (
  args
) => {
  return (
    <Menu {...args}>
      <MenuItem>1.test</MenuItem>
      <MenuItem disabled>2.test2(disabled)</MenuItem>
      <SubMenu title="dropdown">
        <MenuItem>dropdown1</MenuItem>
        <MenuItem>dropdown2</MenuItem>
      </SubMenu>
      <MenuItem>3.test3</MenuItem>
      <MenuItem>4.test4</MenuItem>
    </Menu>
  );
};

export const VerticalMenuWithSubMenu: ComponentStory<typeof Menu> = (args) => {
  return (
    <Menu {...args} mode="vertical" defaultOpenSubMenu={["2"]}>
      <MenuItem>1.test</MenuItem>
      <MenuItem disabled>2.test2(disabled)</MenuItem>
      <SubMenu title="dropdown">
        <MenuItem>dropdown1</MenuItem>
        <MenuItem>dropdown2</MenuItem>
      </SubMenu>
      <SubMenu title="dropdown">
        <MenuItem>dropdown3</MenuItem>
        <MenuItem>dropdown4</MenuItem>
      </SubMenu>
      <MenuItem>3.test3</MenuItem>
      <MenuItem>4.test4</MenuItem>
    </Menu>
  );
};
