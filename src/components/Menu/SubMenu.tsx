import React, { useContext, useState } from "react";
import Transition from "../Transition";
import { MenuContext, IMenuContext } from "./index";
import { IMenuItemProps } from "./MenuItem";
import Icon from "../Icon";

import * as S from "./style";
export interface ISubMenuProps {
  index?: string;
  title?: string;
  className?: string;
  children?: React.ReactNode;
  context?: IMenuContext;
  menuOpen?: boolean;
}
const SubMenu: React.FC<ISubMenuProps> = ({
  index,
  title,
  children,
  className,
}) => {
  const context = useContext(MenuContext);
  const openedSubMenus = context.defaultOpenSubMenu as Array<string>;
  const isOpen =
    index && context.mode === "vertical"
      ? openedSubMenus.includes(index)
      : false;
  const [menuOpen, setMenuOpen] = useState(isOpen);

  const renderChildren = () => {
    const childrenComponent = React.Children.map(children, (child, i) => {
      const childEl = child as React.FunctionComponentElement<IMenuItemProps>;
      if (childEl.type.displayName === "MenuItem") {
        return React.cloneElement(childEl, {
          index: `${index}-${i}`,
        });
      } else {
        console.warn("必須是 MenuItem 組件");
      }
    });
    return (
      <Transition in={menuOpen} timeout={300} animation="zoom-in-top">
        <S.MenuWrap>{childrenComponent}</S.MenuWrap>
      </Transition>
    );
  };

  const handleClick = (e: React.MouseEvent) => {
    e.preventDefault();
    setMenuOpen(!menuOpen);
  };
  let timer: any;
  const handleMouse = (e: React.MouseEvent, toggle: boolean) => {
    clearTimeout(timer);
    e.preventDefault();
    timer = setTimeout(() => {
      setMenuOpen(toggle);
    }, 100);
  };

  const clickEvent =
    context.mode === "vertical"
      ? {
          onClick: handleClick,
        }
      : {};

  const hoverEvent =
    context.mode !== "vertical"
      ? {
          onMouseEnter: (e: React.MouseEvent) => {
            handleMouse(e, true);
          },
          onMouseLeave: (e: React.MouseEvent) => {
            handleMouse(e, false);
          },
        }
      : {};

  return (
    <S.SubMenuWrap
      key={index}
      className={className}
      context={context}
      menuOpen={menuOpen}
      {...hoverEvent}
    >
      <S.SubMenuTitle context={context} {...clickEvent} menuOpen={menuOpen}>
        {title}
        <Icon icon="angle-down"></Icon>
      </S.SubMenuTitle>
      {renderChildren()}
    </S.SubMenuWrap>
  );
};

SubMenu.displayName = "SubMenu";

export default SubMenu;
