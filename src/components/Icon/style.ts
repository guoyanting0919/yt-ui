import styled from "styled-components/macro";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IIconProps } from "./index";

export const FontAwesomeIconWrap = styled(FontAwesomeIcon)<IIconProps>`
  color: ${({ fonttheme, theme }) => {
    if (fonttheme !== "default" && fonttheme !== undefined) {
      return theme.$colors[`$${fonttheme}`];
    } else {
      return "inherit";
    }
  }};
`;
