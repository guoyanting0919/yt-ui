import React, { useContext } from "react";
import { MenuContext, IMenuContext } from "./index";
import * as S from "./style";

export interface IMenuItemProps {
  index?: string;
  disabled?: boolean;
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
  context?: IMenuContext;
  menuOpen?: boolean;
}

const MenuItem: React.FC<IMenuItemProps> = (props) => {
  const { children } = props;
  const context = useContext(MenuContext);
  const handleClick = () => {
    if (
      context.onSelect &&
      !props.disabled &&
      typeof props.index === "string"
    ) {
      context.onSelect(props.index);
    }
  };

  return (
    <S.MenuItemWrap onClick={handleClick} context={context} {...props}>
      {children}
    </S.MenuItemWrap>
  );
};

MenuItem.displayName = "MenuItem";
export default MenuItem;
