import styled from "styled-components/macro";

import { IMenuProps } from "./index";
import { IMenuItemProps } from "./MenuItem";
import { ISubMenuProps } from "./SubMenu";

export const MenuWrap = styled.ul<IMenuProps>`
  list-style: none;
  flex-wrap: wrap;
  display: flex;
  box-shadow: ${({ theme }) => theme.$menu.$menuBoxShadow};
  border-bottom: ${({ theme }) => theme.$menu.$menuBorderBottom};
  flex-direction: ${({ mode }) => (mode === "horizontal" ? "row" : "column")};
  align-items: ${({ mode }) =>
    mode === "horizontal" ? "center" : "flex-start"};
`;

export const MenuItemWrap = styled.li<IMenuItemProps>`
  cursor: pointer;
  transition: 0.5s;
  border-style: solid;
  padding: ${({ theme }) => theme.$menu.$menuItemPadding};
  pointer-events: ${({ disabled }) => (disabled ? "none" : "")};
  border-bottom-width: ${({ theme, context }) =>
    context?.mode === "horizontal"
      ? theme.$menu.$menuItemActiveBorderBottomWidth
      : null};
  border-left-width: ${({ theme, context }) =>
    context?.mode === "horizontal"
      ? null
      : theme.$menu.$menuItemActiveBorderBottomWidth};
  border-color: ${({ theme, context, index }) => {
    if (index === context?.index) {
      return theme.$colors.$primary;
    } else {
      return "transparent";
    }
  }};
  color: ${({ index, context, disabled, theme }) => {
    if (disabled) return theme.$commonColors.$gray;
    if (index === context?.index) {
      return theme.$colors.$primary;
    } else {
      return theme.$commonColors.$black;
    }
  }};
  &:hover,
  &:focus {
    border-style: solid;
    border-color: ${({ theme }) => theme.$colors.$primary};
    color: ${({ theme }) => theme.$colors.$primary};
    border-bottom-width: ${({ theme, context }) =>
      context?.mode === "horizontal"
        ? theme.$menu.$menuItemActiveBorderBottomWidth
        : null};
    border-left-width: ${({ theme, context }) =>
      context?.mode === "horizontal"
        ? null
        : theme.$menu.$menuItemActiveBorderBottomWidth};
  }
`;

export const SubMenuWrap = styled.ul<ISubMenuProps>`
  position: relative;
  ul {
    transition: 0.3s;
    /* display: ${({ menuOpen }) => (menuOpen ? "block" : "none")}; */
    /* opacity: ${({ menuOpen }) => (menuOpen ? 1 : 0)}; */
    border: none;
    position: ${({ context }) =>
      context?.mode === "horizontal" ? "absolute" : "relative"};
    padding-left: ${({ context }) =>
      context?.mode === "horizontal" ? null : "16px"};
    box-shadow: ${({ context, theme }) =>
      context?.mode === "horizontal"
        ? `0 0 3px ${theme.$commonColors.$gray}`
        : null};
  }
`;

export const SubMenuTitle = styled(MenuItemWrap)`
  display: flex;
  align-items: center;
  &:hover svg {
    transform: ${({ context }) =>
      context?.mode === "vertical" ? null : "rotate(180deg)"};
  }
  svg {
    margin-left: 8px;
    transition: 0.5s;
    transform: ${({ context, menuOpen }) =>
      menuOpen && context?.mode === "vertical" ? "rotate(180deg)" : "inherit"};
  }
`;
