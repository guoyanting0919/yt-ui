import styled from "styled-components/macro";
import { tint } from "polished";
import { IButtonProps } from "./index";

export const ButtonWrap = styled.button<IButtonProps>`
  text-align: center;
  box-shadow: none;
  margin-right: 8px;
  margin-bottom: 4px;
  border-radius: ${({ rounded }) => (rounded ? "100px" : "4px")};
  padding: 4px 16px;
  transition: 0.5s;
  cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
  opacity: ${({ disabled, theme }) =>
    disabled ? theme.$button.$buttonDisabledOpacity : 1};
  font-size: ${({ theme, size }) => {
    switch (size) {
      case "lg":
        return theme.$button.$buttonLargeFontSize;
      case "small":
        return theme.$button.$buttonSmallFontSize;
      default:
        return theme.$button.$buttonDefaultFontSize;
    }
  }};
  background-color: ${({ btnType, theme, outline }) => {
    if (btnType !== undefined)
      switch (btnType) {
        case "link":
          return "transparent";
        default:
          return outline ? "transparent" : theme.$colors[`$${btnType}`];
      }
  }};
  color: ${({ btnType, theme, outline }) => {
    if (btnType !== undefined)
      switch (btnType) {
        case "link":
          return theme.$colors.$primary;
        default:
          return outline ? theme.$colors[`$${btnType}`] : theme.$colors.$light;
      }
  }};
  border: ${({ btnType, theme, outline }) => {
    if (btnType !== undefined)
      switch (btnType) {
        case "link":
          return "none";
        default:
          return outline
            ? `${theme.$button.$buttonBorder} solid ${
                theme.$colors[`$${btnType}`]
              }`
            : `${theme.$button.$buttonBorder} solid transparent`;
      }
  }};
  &:hover,
  &:active {
    background-color: ${({ btnType, theme, outline, disabled }) => {
      if (btnType !== undefined && btnType !== "link")
        if (!disabled)
          switch (btnType) {
            default:
              return outline
                ? theme.$colors[`$${btnType}`]
                : tint(0.3, theme.$colors[`$${btnType}`]);
          }
    }};
    color: ${({ theme, disabled }) => (disabled ? null : theme.$colors.$light)};
  }
  & > {
    pointer-events: ${({ disabled }) => (disabled ? "none" : "inherit")};
  }
`;

export const AWrap = styled.a<IButtonProps>`
  transition: 0.5s;
  cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
  opacity: ${({ disabled, theme }) =>
    disabled ? theme.$button.$buttonDisabledOpacity : 1};
  pointer-events: ${({ disabled }) => (disabled ? "none" : "inherit")};
  &:visited {
    color: ${({ theme }) => theme.$colors.$primary};
  }
  &:hover {
    color: red;
    text-decoration: underline;
    color: ${({ theme }) => theme.$colors.$primary};
  }
`;

export const IconWrap = styled.i<IButtonProps>`
  margin-right: ${({ iconPosition, size }) => {
    if (iconPosition !== "start") return;
    if (size !== "small") return "8px";
    if (size === "small") return "4px";
  }};
  margin-left: ${({ iconPosition, size }) => {
    if (iconPosition !== "end") return;
    if (size !== "small") return "8px";
    if (size === "small") return "4px";
  }};
`;
