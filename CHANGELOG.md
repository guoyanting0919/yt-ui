# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.3 (2022-06-22)

### Features

- Button component add icon & icon position prop ([0878741](https://gitlab.com/guoyanting0919/yt-ui/commit/0878741d5c931bbb924cbe205bf082fa745c0bc7))
- Button component add rounded prop ([adeef66](https://gitlab.com/guoyanting0919/yt-ui/commit/adeef6628b4eea500eb38e0014947704e200dd92))
- Button sb ([591af6e](https://gitlab.com/guoyanting0919/yt-ui/commit/591af6e4916f67a56a175072bc1503a87b6d29c6))
- Button storybook ([4a5a41c](https://gitlab.com/guoyanting0919/yt-ui/commit/4a5a41c3ca2334fc67ee37c27c9cd39d5a39dab9))
- Done Button component ([206e629](https://gitlab.com/guoyanting0919/yt-ui/commit/206e629589687c6bb1448c249dc4be163e37587c))
- Done Icon component ([82af03f](https://gitlab.com/guoyanting0919/yt-ui/commit/82af03f94011c5afb52c94884de0d2c98df5683c))
- Done Menu component ([badc09b](https://gitlab.com/guoyanting0919/yt-ui/commit/badc09b7da5f5807d46c13b44aefad7238a67c56))
- Done transition component ([3e587e5](https://gitlab.com/guoyanting0919/yt-ui/commit/3e587e52e588fe0c7ac63995e2ff367778ea3b09))
- Icon storybook ([4e1666e](https://gitlab.com/guoyanting0919/yt-ui/commit/4e1666e09be83412de286e319e1fb4a4d5d0a1d4))
- Menu component storybook ([857d432](https://gitlab.com/guoyanting0919/yt-ui/commit/857d432beffb078ea9ce5c006cc916b88b16d986))
- Transition component adon storybook ([0a961b2](https://gitlab.com/guoyanting0919/yt-ui/commit/0a961b274fb96bd937dbf376f5919a7438aac81d))

### Bug Fixes

- Button component style ([f130e7f](https://gitlab.com/guoyanting0919/yt-ui/commit/f130e7f43202e71062e6d93be1703b930b530dcf))

### [0.1.2](https://gitlab.com/guoyanting0919/yt-ui/compare/v0.1.1...v0.1.2) (2022-06-22)

### 0.1.1 (2022-06-22)

### Features

- Button component add icon & icon position prop ([0878741](https://gitlab.com/guoyanting0919/yt-ui/commit/0878741d5c931bbb924cbe205bf082fa745c0bc7))
- Button component add rounded prop ([adeef66](https://gitlab.com/guoyanting0919/yt-ui/commit/adeef6628b4eea500eb38e0014947704e200dd92))
- Button sb ([591af6e](https://gitlab.com/guoyanting0919/yt-ui/commit/591af6e4916f67a56a175072bc1503a87b6d29c6))
- Button storybook ([4a5a41c](https://gitlab.com/guoyanting0919/yt-ui/commit/4a5a41c3ca2334fc67ee37c27c9cd39d5a39dab9))
- Done Button component ([206e629](https://gitlab.com/guoyanting0919/yt-ui/commit/206e629589687c6bb1448c249dc4be163e37587c))
- Done Icon component ([82af03f](https://gitlab.com/guoyanting0919/yt-ui/commit/82af03f94011c5afb52c94884de0d2c98df5683c))
- Done Menu component ([badc09b](https://gitlab.com/guoyanting0919/yt-ui/commit/badc09b7da5f5807d46c13b44aefad7238a67c56))
- Done transition component ([3e587e5](https://gitlab.com/guoyanting0919/yt-ui/commit/3e587e52e588fe0c7ac63995e2ff367778ea3b09))
- Icon storybook ([4e1666e](https://gitlab.com/guoyanting0919/yt-ui/commit/4e1666e09be83412de286e319e1fb4a4d5d0a1d4))
- Menu component storybook ([857d432](https://gitlab.com/guoyanting0919/yt-ui/commit/857d432beffb078ea9ce5c006cc916b88b16d986))
- Transition component adon storybook ([0a961b2](https://gitlab.com/guoyanting0919/yt-ui/commit/0a961b274fb96bd937dbf376f5919a7438aac81d))

### Bug Fixes

- Button component style ([f130e7f](https://gitlab.com/guoyanting0919/yt-ui/commit/f130e7f43202e71062e6d93be1703b930b530dcf))

# 0.1.0 (2022-06-22)

### Bug Fixes

- Button component style ([f130e7f](https://gitlab.com/guoyanting0919/yt-ui/commit/f130e7f43202e71062e6d93be1703b930b530dcf))

### Features

- Button component add icon & icon position prop ([0878741](https://gitlab.com/guoyanting0919/yt-ui/commit/0878741d5c931bbb924cbe205bf082fa745c0bc7))
- Button component add rounded prop ([adeef66](https://gitlab.com/guoyanting0919/yt-ui/commit/adeef6628b4eea500eb38e0014947704e200dd92))
- Button sb ([591af6e](https://gitlab.com/guoyanting0919/yt-ui/commit/591af6e4916f67a56a175072bc1503a87b6d29c6))
- Button storybook ([4a5a41c](https://gitlab.com/guoyanting0919/yt-ui/commit/4a5a41c3ca2334fc67ee37c27c9cd39d5a39dab9))
- Done Button component ([206e629](https://gitlab.com/guoyanting0919/yt-ui/commit/206e629589687c6bb1448c249dc4be163e37587c))
- Done Icon component ([82af03f](https://gitlab.com/guoyanting0919/yt-ui/commit/82af03f94011c5afb52c94884de0d2c98df5683c))
- Done Menu component ([badc09b](https://gitlab.com/guoyanting0919/yt-ui/commit/badc09b7da5f5807d46c13b44aefad7238a67c56))
- Done transition component ([3e587e5](https://gitlab.com/guoyanting0919/yt-ui/commit/3e587e52e588fe0c7ac63995e2ff367778ea3b09))
- Icon storybook ([4e1666e](https://gitlab.com/guoyanting0919/yt-ui/commit/4e1666e09be83412de286e319e1fb4a4d5d0a1d4))
- Menu component storybook ([857d432](https://gitlab.com/guoyanting0919/yt-ui/commit/857d432beffb078ea9ce5c006cc916b88b16d986))
- Transition component adon storybook ([0a961b2](https://gitlab.com/guoyanting0919/yt-ui/commit/0a961b274fb96bd937dbf376f5919a7438aac81d))

### Performance Improvements

- add custom decorators for Menu component & remove global decorators ([c54da07](https://gitlab.com/guoyanting0919/yt-ui/commit/c54da072ffa9f51cded0e8432a2ff2e60c43e1ab))
