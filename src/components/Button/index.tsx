import React from "react";
import classNames from "classnames";
import * as S from "./style";
import Icon from "../Icon";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

export type ButtonSize = "lg" | "small" | "default";

export type ButtonType =
  | "primary"
  | "danger"
  | "info"
  | "success"
  | "link"
  | "warning"
  | "secondary";

type IconPosition = "start" | "end";

export interface BaseButtonProps {
  className?: string;
  disabled?: boolean;
  /** 是否啟用 outline 樣式 */
  outline?: boolean;
  size?: ButtonSize;
  btnType?: ButtonType;
  children?: React.ReactNode;
  /** 當 btnType 設置為 link 時生效，其屬性等同於 a 標籤 */
  href?: string;
  rounded?: boolean;
  /** icon 位置 */
  iconPosition?: IconPosition;
  /** icon 圖標 base on Icon component */
  icon?: IconProp;
}
type NativeButtonProps = BaseButtonProps &
  React.ButtonHTMLAttributes<HTMLElement>;
type AnchorButtonProps = BaseButtonProps &
  React.AnchorHTMLAttributes<HTMLElement>;
export type IButtonProps = Partial<NativeButtonProps & AnchorButtonProps>;

const Button: React.FC<IButtonProps> = (props) => {
  const {
    className,
    disabled,
    size,
    btnType,
    children,
    href,
    icon,
    iconPosition,
  } = props;
  const classes = classNames("btn", className, {
    [`btn-${btnType}`]: btnType,
    [`btn-${size}`]: size,
    disabled: disabled,
  });

  if (btnType === "link" && href) {
    return (
      <S.AWrap {...props} className={classes} href={href}>
        {children}
      </S.AWrap>
    );
  } else {
    return (
      <S.ButtonWrap {...props} className={classes}>
        <>
          {icon && iconPosition === "start" && (
            <S.IconWrap {...props}>
              <Icon icon={icon} />
            </S.IconWrap>
          )}
          {children}
          {icon && iconPosition === "end" && (
            <S.IconWrap {...props}>
              <Icon icon={icon} />
            </S.IconWrap>
          )}
        </>
      </S.ButtonWrap>
    );
  }
};

Button.defaultProps = {
  disabled: false,
  btnType: "primary",
  outline: false,
  rounded: false,
  iconPosition: "start",
  size: "default",
};

export default Button;
