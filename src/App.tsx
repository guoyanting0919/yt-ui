import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle } from "./components/globalStyle";
import { darkTheme, lightTheme } from "./themes";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

const App: React.FC = () => {
  return (
    <ThemeProvider theme={lightTheme}>
      <GlobalStyle />
      <div className="App">
        <iframe
          style={{ width: "100%", height: "calc(100vh - 8px)" }}
          title="sb"
          src="http://localhost:6006"
        ></iframe>
      </div>
    </ThemeProvider>
  );
};

export default App;
