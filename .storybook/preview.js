import React from "react";

import { ThemeProvider } from "styled-components";
import { darkTheme, lightTheme } from "../src/themes";
import { GlobalStyle } from "../src/components/globalStyle";

// icon style
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fas);

// custom canvas style
const style = {
  // minHeight: "200px",
};
export const decorators = [
  (Story) => (
    <div style={style}>
      <GlobalStyle />
      <ThemeProvider theme={lightTheme}>
        <Story />
      </ThemeProvider>
    </div>
  ),
];

export const parameters = {
  actions: { argTypesRegex: "^on.*" },
  backgrounds: {
    values: [
      { name: "darkBg", value: "#f00" },
      { name: "lightTheme", value: "#0f0" },
    ],
  },
};
