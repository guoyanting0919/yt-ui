import React, { createContext, useState } from "react";
import { IMenuItemProps } from "./MenuItem";
import * as S from "./style";

type MenuMode = "horizontal" | "vertical";
type SelectCallBack = (selectIndex: string) => void;

export interface IMenuContext {
  index?: string;
  onSelect?: SelectCallBack;
  mode?: MenuMode;
  defaultOpenSubMenu?: string[];
}
export interface IMenuProps {
  className?: string;
  /** default height light 之 menuItem or subMenu */
  defaultIndex?: string;
  /** 水平模式 or 垂直模式 */
  mode?: MenuMode;
  style?: React.CSSProperties;
  onSelect?: SelectCallBack;
  children?: React.ReactNode;
  /** 當 mode 為 vertical 時可設置，預設打開所設定之 submenu */
  defaultOpenSubMenu?: string[];
}

export const MenuContext = createContext<IMenuContext>({ index: "0" });

const Menu: React.FC<IMenuProps> = (props) => {
  const {
    defaultIndex,
    onSelect,
    children,
    mode,
    className,
    style,
    defaultOpenSubMenu,
  } = props;
  const [currentActive, setCurrentActive] = useState(defaultIndex);
  const handleClick = (index: string) => {
    setCurrentActive(index);
    if (onSelect) {
      onSelect(index);
    }
  };
  const passContext: IMenuContext = {
    index: currentActive ? currentActive : "0",
    mode: props.mode,
    onSelect: handleClick,
    defaultOpenSubMenu,
  };
  const renderChildren = () => {
    return React.Children.map(children, (child, index) => {
      const childEl = child as React.FunctionComponentElement<IMenuItemProps>;
      const { displayName } = childEl.type;
      if (displayName === "MenuItem" || displayName === "SubMenu") {
        return React.cloneElement(childEl, { index: index.toString() });
      } else {
        console.warn("必須是 MenuItem 組件");
      }
    });
  };
  return (
    <S.MenuWrap className={className} style={style} mode={mode}>
      <MenuContext.Provider value={passContext}>
        {renderChildren()}
      </MenuContext.Provider>
    </S.MenuWrap>
  );
};

Menu.defaultProps = {
  defaultIndex: "0",
  mode: "horizontal",
  defaultOpenSubMenu: [],
};

export default Menu;
