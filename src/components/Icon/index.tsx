import React from "react";
import { FontAwesomeIconProps } from "@fortawesome/react-fontawesome";
import * as S from "./style";

export type FontThemeProps =
  | "primary"
  | "secondary"
  | "danger"
  | "info"
  | "warning"
  | "success"
  | "default";

export interface IIconProps extends FontAwesomeIconProps {
  fonttheme?: FontThemeProps;
}

const Icon: React.FC<IIconProps> = (props) => {
  const { className, fonttheme, ...restProps } = props;
  return (
    <S.FontAwesomeIconWrap
      className={className}
      fonttheme={fonttheme}
      {...restProps}
    ></S.FontAwesomeIconWrap>
  );
};

Icon.defaultProps = {
  fonttheme: "default",
};

export default Icon;
