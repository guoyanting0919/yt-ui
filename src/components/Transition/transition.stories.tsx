import React, { useState } from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import Transition from "./index";
import Button from "../Button";

export default {
  title: "yt-ui/Transition",
  component: Transition,
  subcomponents: {},
  decorators: [
    (Story) => (
      <div style={{ minHeight: "100px" }}>
        <Story />
      </div>
    ),
  ],
} as ComponentMeta<typeof Transition>;

export const BasicTransition: ComponentStory<typeof Transition> = (args) => {
  const [toggle, setToggle] = useState<boolean>(true);
  return (
    <>
      <div>
        <Button
          btnType="primary"
          onClick={() => {
            setToggle(!toggle);
          }}
        >
          toggle
        </Button>
      </div>
      <Transition
        animation={"zoom-in-bottom"}
        in={toggle}
        timeout={300}
        {...args}
        wrapper
        style={{ display: "inline-block" }}
      >
        <div>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
          <p>BasicTransition</p>
        </div>
      </Transition>
    </>
  );
};

export const TransitionWithoutWrapper: ComponentStory<typeof Transition> = (
  args
) => {
  const [toggle, setToggle] = useState<boolean>(true);
  return (
    <>
      <div>
        <Button
          btnType="primary"
          onClick={() => {
            setToggle(!toggle);
          }}
        >
          toggle
        </Button>
      </div>
      <Transition
        animation={"zoom-in-bottom"}
        in={toggle}
        timeout={300}
        {...args}
        style={{ display: "inline-block" }}
      >
        <>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
          <p>TransitionWithoutWrapper</p>
        </>
      </Transition>
    </>
  );
};
